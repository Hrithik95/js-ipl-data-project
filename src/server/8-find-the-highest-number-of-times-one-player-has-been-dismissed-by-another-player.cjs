//Find the highest number of times one player has been dismissed by another player.

const deliveriesPath = '/home/hrithik/Desktop/js-ipl-data-project/src/data/deliveries.csv';
const fs = require('fs');
const csvToJson = require('csvtojson');

csvToJson()
    .fromFile(deliveriesPath)
    .then((deliveries) => {
        const batsmenDismissalData = deliveries.reduce((acc, event) => {
            if (event.player_dismissed !== '') {
                if(!acc[event.batsman]){
                    acc[event.batsman] = {};
                }
                if(!acc[event.batsman][event.bowler]){
                    acc[event.batsman][event.bowler] = 0;
                }
                acc[event.batsman][event.bowler] += 1;
                return acc;
            }

            return acc;
        }, {});

        const batsmenDismissalDataArray = Object.entries(batsmenDismissalData); 
        let answer = [];
        batsmenDismissalDataArray.map((item) => {
            let dataToBeSorted = Object.entries(item[1]);
            let sortedData = dataToBeSorted.sort((prev, curr) => {
                //[1] contains the bowler name and no. of times he dismissed a particular batsman
                if (prev[1] < curr[1]) {
                    return 1;
                } else if (prev[1] > curr[1]) {
                    return -1;
                } else {
                    return 0;
                }
            });
            let bowlerWithMostDismissal = sortedData.slice(0, 1);
            answer.push({
                batsman: item[0],
                dismissedMostTimesBy: bowlerWithMostDismissal[0]
            });
        });
        fs.writeFileSync('/home/hrithik/Desktop/js-ipl-data-project/src/public/output/8-highest-number-of-times-one-player-dismissed-by-another-player.json', JSON.stringify(answer, null, 2));
    });



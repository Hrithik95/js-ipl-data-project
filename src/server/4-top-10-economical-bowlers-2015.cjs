//Top 10 economical bowlers in the year 2015
const csvToJson = require('csvtojson');
const fs = require('fs');
const matchesPath = '/home/hrithik/Desktop/js-ipl-data-project/src/data/matches.csv';
const deliveriesPath = '/home/hrithik/Desktop/js-ipl-data-project/src/data/deliveries.csv';

let matchesDataId = [];
csvToJson()
    .fromFile(matchesPath)
    .then((matchesData) => {
        matchesData.map((item) => {
            if (item.season === "2015") {
                matchesDataId.push(item.id);
            }
        });
        // console.log(matchesDataId);
    });
csvToJson()
    .fromFile(deliveriesPath)
    .then((deliveriesData) => {
        let dataArr = deliveriesData.filter((item) => {
            return matchesDataId.includes(item.match_id);
        });
        let bowelerData = dataArr.reduce((acc, entry) => {
            if (!acc[entry.bowler]) {
                acc[entry.bowler] = {
                    runs: 0,
                    ball: 0,
                    eco: 0
                }
            }
            acc[entry.bowler].runs += Number(entry.total_runs);
            acc[entry.bowler].ball += 1;
            let economy = (acc[entry.bowler].runs / acc[entry.bowler].ball) * 6;
            acc[entry.bowler].eco = economy;
            return acc;

        }, {});
        // console.log(bowelerData);
        let answer = Object.entries(bowelerData);
        // console.log(answer);
        let sortedAccToEco = answer.sort((prev, curr) => {
            if (prev[1].eco > curr[1].eco) {
                return 1;
            }
            else if (prev[1].eco < curr[1].eco) {
                return -1;
            }
            else {
                return 0;
            }
        });
        let top10EcoBowler = sortedAccToEco.slice(0, 10);
        // console.log(top10EcoBowler);
        fs.writeFileSync('/home/hrithik/Desktop/js-ipl-data-project/src/public/output/4-top-10-economical-bowler-2015.json', JSON.stringify(top10EcoBowler, null, 2));
    });

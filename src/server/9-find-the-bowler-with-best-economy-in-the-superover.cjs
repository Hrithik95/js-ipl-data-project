//Find the bowler with the best economy in super overs
const deliveriesPath = '/home/hrithik/Desktop/js-ipl-data-project/src/data/deliveries.csv';
const fs = require('fs');
const csvToJson = require('csvtojson');

csvToJson()
    .fromFile(deliveriesPath)
    .then((deliveries) => {
        let superOverEconomy = deliveries.reduce((acc,event)=>{
            if(event.is_super_over != 0){
                if(!acc[event.bowler]){
                    acc[event.bowler] = {
                        runs:Number(event.total_runs),
                        balls:1,
                        eco:0
                    }
                }else{
                    acc[event.bowler].runs += Number(event.total_runs);
                    acc[event.bowler].balls += 1;
                    let economy = (acc[event.bowler].runs/acc[event.bowler].balls)*6;
                    acc[event.bowler].eco = economy;
                }
            }
            return acc;
        },{});
        // console.log(superOverEconomy);
        let answer = Object.entries(superOverEconomy);
        // console.log(answer);
        let superOverEconomyArr = answer.sort((prev, curr) => {
            if (prev[1].eco > curr[1].eco) {
                return 1;
            }
            else if (prev[1].eco < curr[1].eco) {
                return -1;
            }
            else {
                return 0;
            }
        });
        let bestBowler = superOverEconomyArr.slice(0,1);
        // console.log(bestBowler);

        fs.writeFileSync('/home/hrithik/Desktop/js-ipl-data-project/src/public/output/9-bowlers-with-best-economy-in-superovers.json',JSON.stringify(bestBowler,null,2));

    });
//Find the strike rate of a batsman for each season
const csvToJson = require('csvtojson');
const fs = require('fs');
const matchesPath = '/home/hrithik/Desktop/js-ipl-data-project/src/data/matches.csv';
const deliveriesPath = '/home/hrithik/Desktop/js-ipl-data-project/src/data/deliveries.csv';

let matchId;
csvToJson()
    .fromFile(matchesPath)
    .then((matchData) => {
        matchId = matchData.map((item) => {
            return {
                id: item.id,
                season: item.season
            };
        });
    });

csvToJson()
    .fromFile(deliveriesPath)
    .then((deliveryData) => {
        const mergedDataset = deliveryData.map((item) => {
            const yearObject = matchId.find(
                (match) => match.id == String(item.match_id)
            );
            return {
                ...item,
                Year: yearObject.season
            };
        });

        const batsmenData = mergedDataset.reduce((acc, event) => {
            const batsman = event.batsman;
            const runs = Number(event.batsman_runs);
            const season = event.Year;

            if (!acc[batsman]) {
                acc[batsman] = {};
            }
            if (!acc[batsman][season]) {
                acc[batsman][season] = {
                    runs_scored: runs,
                    balls_faced: 1,
                    strike_rate: undefined,
                }
            } else {
                if (!acc[batsman][season].strike_rate) {
                    acc[batsman][season].strike_rate = (acc[batsman][season].runs_scored / acc[batsman][season].balls_faced) * 100;
                }
                acc[batsman][season].runs_scored += runs;
                acc[batsman][season].balls_faced++;
                acc[batsman][season].strike_rate = (acc[batsman][season].runs_scored / acc[batsman][season].balls_faced) * 100;
            }

            return acc;
        }, {});
        // console.log(batsmenData);
        fs.writeFileSync('/home/hrithik/Desktop/js-ipl-data-project/src/public/output/7-strike-rate-of-batsmen-each-season.json', JSON.stringify(batsmenData, null, 2));
    });



// Find a player who has won the highest number of Player of the Match awards for each season

const csvToJson = require('csvtojson');
const fs = require('fs');
const matchesPath = '/home/hrithik/Desktop/js-ipl-data-project/src/data/matches.csv';

csvToJson()
    .fromFile(matchesPath)
    .then((data) => {
        //calculating the number of times each player has won Player of the Match in each season.
        let dataOfPlayerOfMatch = data.reduce((acc, entry) => {

            if (!acc[entry.season]) {
                acc[entry.season] = {};
            }
            if (acc[entry.season]) {
                let playerOfMatch = entry["player_of_match"];
                if (!acc[entry.season][playerOfMatch]) {
                    acc[entry.season][playerOfMatch] = 1;
                } else {
                    acc[entry.season][playerOfMatch] += 1;
                }
            }
            return acc;
        }, {});
        //converting the data present in form of object into array format to easily sort the data.
        let playerOfTheMatchArray = Object.entries(dataOfPlayerOfMatch);
        let answerArray = [];
        //mapping through the array to get the final result.
        playerOfTheMatchArray.map((item) => {
            let dataToBeSorted = Object.entries(item[1]);
            //sorting the data in ascending order.
            let sortedData = dataToBeSorted.sort((prev, curr) => {
                if (prev[1] < curr[1]) {
                    return 1;
                } else if (prev[1] > curr[1]) {
                    return -1;
                } else {
                    return 0;
                }
            });
            //obtaining the player with highest player of the match in each season.
            let highestMOM = sortedData.slice(0, 1);
            answerArray.push({
                year: item[0],
                highestPlayerOfMatch: highestMOM[0]
            });
        });
        // console.log(answerArray);

        // getting output to new .json file
        fs.writeFileSync('/home/hrithik/Desktop/js-ipl-data-project/src/public/output/6-player-won-highest-number-of-player-of-the-match-awards-each-season.json', JSON.stringify(answerArray, null, 2));
    });

//Number of matches played per year for all the years in IPL.

const csvToJson = require('csvtojson');
const fs = require('fs');
const matchesPath = '/home/hrithik/Desktop/js-ipl-data-project/src/data/matches.csv';

csvToJson()
    .fromFile(matchesPath)
    .then((data) => {
        //creating an object to store the result.
        const matchPerYear = {};
        //mapping through the data.
        data.map((item) => {
            if (matchPerYear[item.season] == undefined) {
                matchPerYear[item.season] = 1;
            }
            else {
                matchPerYear[item.season] = (matchPerYear[item.season]) + 1;
            }
        });
        console.log(matchPerYear);
        // getting output to new .json file
        fs.writeFileSync('/home/hrithik/Desktop/js-ipl-data-project/src/public/output/1-matches-per-year.json', JSON.stringify(matchPerYear, null, 2));
    });




//Extra runs conceded per team in the year 2016.
const csvToJson = require('csvtojson');
const fs = require('fs');
const matchesPath = '/home/hrithik/Desktop/js-ipl-data-project/src/data/matches.csv';
const deliveriesPath = '/home/hrithik/Desktop/js-ipl-data-project/src/data/deliveries.csv';

let matchesDataId = [];
csvToJson()
    .fromFile(matchesPath)
    .then((matchesData) => {
        matchesData.map((item) => {
            if (item.season === "2016") {
                matchesDataId.push(item.id);
            }
        });
    });

csvToJson()
    .fromFile(deliveriesPath)
    .then((deliveriesData) => {
        let extraRunsByTeams = {};
        deliveriesData.map((item) => {
            if (matchesDataId.includes(item.match_id)) {
                if (!extraRunsByTeams.hasOwnProperty(item.bowling_team)) {
                    extraRunsByTeams[item.bowling_team] = 0;
                }
                extraRunsByTeams[item.bowling_team] += Number(item.extra_runs);
            }
        });
        fs.writeFileSync('/home/hrithik/Desktop/js-ipl-data-project/src/public/output/3-extra-runs-conceded-per-team.json', JSON.stringify(extraRunsByTeams));
    });
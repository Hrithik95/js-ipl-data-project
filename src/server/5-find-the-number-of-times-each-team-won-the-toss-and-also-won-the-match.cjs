//Find the number of times each team won the toss and also won the match.
const csvToJson = require('csvtojson');
const fs = require('fs');
const matchesPath = '/home/hrithik/Desktop/js-ipl-data-project/src/data/matches.csv';


csvToJson()
    .fromFile(matchesPath)
    .then((data) => {
        let answer = data.reduce((acc, entry) => {
            if (!acc[entry.toss_winner]) {
                acc[entry.toss_winner] = {};
            }
            if (acc[entry.toss_winner] && entry.toss_winner === entry.winner) {
                if (!acc[entry.toss_winner]["toss&match_won"]) {
                    acc[entry.toss_winner]["toss&match_won"] = 1;
                } else {
                    acc[entry.toss_winner]["toss&match_won"] += 1;
                }
            }
            return acc;

        }, {});
        console.log(answer);
        fs.writeFileSync('/home/hrithik/Desktop/js-ipl-data-project/src/public/output/5-find-the-number-of-times-toss-and-match-both-won-by-teams.json', JSON.stringify(answer, null, 2));
    });
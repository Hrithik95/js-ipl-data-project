// Number of matches won per team per year in IPL.

const csvToJson = require('csvtojson');
const fs = require('fs');
const matchesPath = '/home/hrithik/Desktop/js-ipl-data-project/src/data/matches.csv';

csvToJson()
    .fromFile(matchesPath)
    .then((data) => {
        let ansObject = data.reduce((acc, item) => {
            if (!acc[item.winner]) {
                acc[item.winner] = {};
            }
            if (!acc[item.winner][item.season]) {
                acc[item.winner][item.season] = 0;
            }
            acc[item.winner][item.season] += 1;
            return acc;
        }, {});
        fs.writeFileSync('/home/hrithik/Desktop/js-ipl-data-project/src/public/output/2-matches-won-per-team-per-year.json', JSON.stringify(ansObject, null, 2));
    });